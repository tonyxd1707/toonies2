jQuery(document).ready(function ($) {
	$('html').imagesLoaded(function () {
		$(window).on('load resize', function () {
			var boardHeight = $('.board-display').outerHeight();
			if ($(window).height() < boardHeight) {
				$('.page-index1').css({
					"height" : boardHeight + 200
				});
			}
			else {
				$('.page-index1').css({
					"height" : "100vh"
				});
			}
		})
	});
});

jQuery("ul.tabs").each(function() {
	var $active,
		$content,
		$links = jQuery(this).find("a");
	$active = jQuery(
		$links.filter('[href="' + location.hash + '"]')[0] || $links[0]
	);
	$active.parent().addClass("active");
	$content = $($active[0].hash);
	$links.not($active).each(function() {
		jQuery(this.hash).hide();
	});
	jQuery(this).on("click", "a", function(e) {
		$active.parent().removeClass("active");
		$content.hide();
		$active = jQuery(this);
		$content = jQuery(this.hash);
		$active.parent().addClass("active");
		$content.show();
		e.preventDefault();
	});
});

jQuery(function ($) {
	$(".tabs li a img").mouseover(function () {
		$(this).attr("src", $(this).attr("src").replace("-off.png", "-on.png"));
	}).mouseout(function () {
		$(this).attr("src", $(this).attr("src").replace("-on.png", "-off.png"));
	});
});